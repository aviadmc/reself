﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class windowCameraScript : MonoBehaviour {
    public Camera mainCamera;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
    void LateUpdate () {
        transform.rotation = mainCamera.transform.rotation;
        transform.position = mainCamera.transform.position + new Vector3(0, -150, 0);
	}
}
