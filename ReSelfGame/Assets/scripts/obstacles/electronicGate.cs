﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class electronicGate : MonoBehaviour {
    public bool isBlocking;
    
    private Collider col;
    private Light light = null;

	// Use this for initialization
	void Start () {
        col = GetComponents<Collider>()[1];
        //light = GetComponent<Light>();
        if(light != null) {
            light.intensity = 0;
            if(isBlocking) {
                light.color = Color.red;
            }
            else {
                light.color = Color.green;
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerStay(Collider player){
        if (isBlocking && player.CompareTag("Player")) {
            player.attachedRigidbody.AddForce(transform.up * 100);
            col.enabled = true;
            if (light != null)
            {
                light.intensity = 8;
            }
        } else {
        }
    }

    void OnTriggerExit(Collider player){
        if (isBlocking && player.CompareTag("Player")) {
            player.attachedRigidbody.velocity = player.attachedRigidbody.velocity/10;
            player.attachedRigidbody.angularVelocity = Vector3.zero;
            col.enabled = false;
        } else {
        }
        if (light != null) {
            light.intensity = 0;
        }
    }
}
