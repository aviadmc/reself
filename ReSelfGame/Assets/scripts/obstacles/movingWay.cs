﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movingWay : MonoBehaviour {
	private float moveFactor = 0.1f;
    int dir = 1;
	// Use this for initialization
	void Start () {
        int rand = Random.Range(0, 2);
        if(rand == 0) {
            dir = -1;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay(Collider player){
		if(player.CompareTag("Player")){
			Transform playerTrans = player.transform;
			playerTrans.position += dir * transform.up * moveFactor;
		}
	}
}
