﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {
    public Transform player;
    public float maxDistance = 3.0f;
    public float up = 1.5f;
    public float sensivX = 4.0f;
    public float sensivY = 1.0f;
    public float minYAngle = 330f;
    public float maxYAngle = 400f;

    private float dist;
    private float curY = 400.0f;
    private float camRayDistFromCenter = 0.3f;
    private Light backlight;
    private bool turnLight = false;
    private float lightAdding = 0.25f;
    public Texture2D fadeTexture;

    public bool endGame;
    private float alpha = 0.0f;
    private int fadeDir = -1;
    private float fadeSpeed = 0.02f;
    private int drawDepth = -1000;
    private bool fadeOut = false;

    // Use this for initialization
    void Start () {
        dist = maxDistance;
        backlight = GetComponentInChildren<Light>();
        endGame = false;
    }

    void Update(){
        if (!endGame)
        {
            curY -= Input.GetAxis("Mouse Y") * 15;
            curY = Mathf.Max(Mathf.Min(curY, maxYAngle), minYAngle);

            if (turnLight)
            {
                if (backlight.intensity >= 7.5f)
                {
                    lightAdding = -0.25f;
                }
                backlight.intensity += lightAdding;
                if (backlight.intensity <= 0)
                {
                    turnLight = false;
                    lightAdding = 0.25f;
                }
            }
        }
    }

    // Update is called once per frame
    void LateUpdate() {
        if (!endGame)
        {
            Vector3 dir = new Vector3(0, 0, -dist);
            Quaternion rot = Quaternion.Euler(player.rotation.eulerAngles + Quaternion.Euler(curY, 0, 0).eulerAngles);
            transform.position = player.position + rot * dir;
            transform.LookAt(player.position);

            RaycastHit hitsR;
            RaycastHit hitsL;

            Vector3 rayRDIr = (transform.position + (transform.right * camRayDistFromCenter)) - player.position;
            rayRDIr = rayRDIr / rayRDIr.magnitude;
            if (Physics.Raycast(player.position, rayRDIr, out hitsR, dist, 1))
            {
                transform.position = hitsR.point + (transform.forward * 0.03f) - (transform.right * camRayDistFromCenter);
                dist = (transform.position - player.position).magnitude;
            }
            Vector3 rayLDIr = (transform.position - (transform.right * camRayDistFromCenter)) - player.position;
            rayLDIr = rayLDIr / rayLDIr.magnitude;
            if (Physics.Raycast(player.position, rayLDIr, out hitsL, dist, 1))
            {
                transform.position = hitsL.point + (transform.forward * 0.03f) + (transform.right * camRayDistFromCenter);
                dist = (transform.position - player.position).magnitude;
            }

            Debug.DrawRay(player.position, rayLDIr * dist, Color.green);
            Debug.DrawRay(player.position, rayRDIr * dist, Color.red);

            dist = maxDistance;
        }
    }

    public void changeMazeLight(){
        turnLight = true;
    }


    void OnGUI()
    {
        if (endGame)
        {
            StartCoroutine(fadeOutStart());
            //endGame = false;
        }
        if (fadeOut)
        {
            alpha -= fadeDir * fadeSpeed;
            alpha = Mathf.Clamp01(alpha);

            GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);

            GUI.depth = drawDepth;

            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeTexture);
        }
    }

    IEnumerator fadeOutStart() {
        float pauseEndTime = Time.realtimeSinceStartup + 2;

        while (Time.realtimeSinceStartup < pauseEndTime)
        {
            yield return 0;
        }

        fadeOut = true;
    }
}
