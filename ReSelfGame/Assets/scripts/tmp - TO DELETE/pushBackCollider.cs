﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pushBackCollider : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerStay(Collider player){
		Rigidbody playerRB = player.attachedRigidbody;
        playerRB.AddForce(Vector3.back * 40);
    }

    void OnTriggerExit(Collider player){
		Rigidbody playerRB = player.attachedRigidbody;
        playerRB.AddForce(Vector3.forward * 80);
    }
}
