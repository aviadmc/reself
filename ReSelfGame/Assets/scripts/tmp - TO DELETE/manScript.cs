﻿using UnityEngine;
using System.Collections;

public class manScript : MonoBehaviour {
    public float walkSpeed;
    public float runSpeed;
    public float gravityFactor = 8;
    Rigidbody rb;
    int jumpFlag;

    bool is_walk;
    bool is_right;
    bool is_left;
    bool is_back;
    bool is_jump;

    // Use this for initialization
    void Start ()
    {
        rb = GetComponent<Rigidbody>();
        jumpFlag = 0;
    }

    // Update is called once per frame
    void FixedUpdate ()
    {
        is_walk = Input.GetKey(KeyCode.W);
        is_right = Input.GetKey(KeyCode.D);
        is_left = Input.GetKey(KeyCode.A);
        is_back = Input.GetKey(KeyCode.S);
        is_jump = Input.GetKey(KeyCode.Space);

        transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);

        turn();
        walk();
        jump();
        //fall();
    }

    void fall()
    {
        //temp. I want to add the ability to have more than 6 directions.
        Vector3 pos = new Vector3();

        rb.AddForce((pos) * 70 * Time.smoothDeltaTime);

    }

    void turn()
    {
        transform.Rotate(0, Input.GetAxis("Mouse X") * 20, 0);
    }

    void walk()
    {
        var curSpeed = Input.GetKey(KeyCode.LeftShift) ? runSpeed : walkSpeed;
        if (is_walk)
        {
            transform.Translate(Vector3.forward * curSpeed * Time.deltaTime);
        }
        else if (is_right)
        {
            transform.Translate(Vector3.right * curSpeed * Time.deltaTime);
        }
        else if (is_left)
        {
            transform.Translate(-Vector3.right * curSpeed * Time.deltaTime);
        }
        else if (is_back)
        {
            transform.Translate(-Vector3.forward * walkSpeed * 0.6f * Time.deltaTime);
        }
    }

    void jump()
    {
        if (is_jump && jumpFlag == 0)
        {
            //rb.velocity = new Vector3(0, 5, 0);
            rb.velocity = transform.up * 5;
            jumpFlag = 80;
        }
        else if (jumpFlag > 0)
        {
            jumpFlag -= 1;
        }
    }
}
