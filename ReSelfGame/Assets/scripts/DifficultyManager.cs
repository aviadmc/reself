﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyManager : MonoBehaviour {
 public float curDiff = 1;    //we use this to know how hard it sould be now.
 public float nextMaxDiff = 10;

 private int dir = 1;
 private float downHill;
 private float upHill;
 private float psuedoMax = 20;

	// Use this for initialization
	void Start (){
  upHill = nextMaxDiff;
  downHill = upHill / 2;
	}

 //we call this when we need to start the difficulty from the start (e.g., after the white room)
 public void reset(){
  curDiff = 1;
  nextMaxDiff = upHill;
  psuedoMax = 2 * nextMaxDiff;
 }
	
	// Update is called once per frame
	void Update () {
  curDiff += dir * 0.005f;
  if (dir > 0 && curDiff >= nextMaxDiff) {
   dir = -dir;
   nextMaxDiff = nextMaxDiff - downHill;
  } else if (dir < 0 && curDiff <= nextMaxDiff) {
   dir = -dir;
   nextMaxDiff = nextMaxDiff + upHill;
  }

  //we need the psuedoMax variable to be always hugher than curDiff:
  if (psuedoMax <= curDiff) {
   psuedoMax = curDiff + upHill;
  }//this way, we will get closer and closer to 1 at "getObstacleProb"

  createBlackFigurs();
  addDeadEnds();
  changeMaze();
	}

 public float getObstacleProb(){
		return 2*curDiff/psuedoMax;
 }

 public void helpFrank(){
  curDiff = curDiff / 3;
  nextMaxDiff = curDiff + downHill;
 }

 private void createBlackFigurs(){
  //TODO: currently, we have no black figures. Whene we do - we sould implement this
 }

 private void addDeadEnds(){
  //TODO: talk with Yagel.
 }

 private void changeMaze(){
  //TODO: talk with Yagel.
 }
}