﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class windowScript : MonoBehaviour {
    public Transform demoWindow;
    public Camera demoCamera;

    private Vector2[] uvs;
    private Mesh mesh;
    private Mesh demoWindowMesh;
    private Vector3[] vertices;
    private Vector3[] demoWindowVertices;

    private Vector3[] demoWinCorners;
    private Renderer demoWindoRenderer;
    private RenderTexture rt;

    void Start()
    {
        //rt = demoCamera.targetTexture;
        //rt.wrapMode = TextureWrapMode.Repeat;
        mesh = GetComponent<MeshFilter>().mesh;
        vertices = mesh.vertices;
        uvs = new Vector2[vertices.Length];


        demoWindowMesh = demoWindow.GetComponent<MeshFilter>().mesh;
        demoWindowVertices = demoWindowMesh.vertices;
        for (int i = 0; i < vertices.Length; i++) {
            //if (gameObject.name == "Plane (1)" || gameObject.name == "windowTry") {
            demoWindowVertices[i] = new Vector3(demoWindowVertices[i].x * demoWindow.lossyScale.x, -demoWindowVertices[i].y * demoWindow.lossyScale.y, demoWindowVertices[i].z * demoWindow.lossyScale.z) + demoWindow.position;
            //} else {
            //    float x = demoWindowVertices[i].x * demoWindow.lossyScale.x + demoWindow.position.x;
            //    float y = demoWindowVertices[i].y * demoWindow.lossyScale.y + demoWindow.position.y;
            //    float z = demoWindowVertices[i].z * demoWindow.lossyScale.z + demoWindow.position.z;
            //    demoWindowVertices[i] = new Vector3(x, y, z);
            //}
        }

        //mesh.uv = uvs;
    }

    void LateUpdate(){
        for (int i = 0; i < vertices.Length; i++) {
            Vector3 point = demoCamera.WorldToViewportPoint(demoWindowVertices[i]);
            uvs[i] = new Vector2(point.x, point.y);
        }
        mesh.uv = uvs;
    }
}