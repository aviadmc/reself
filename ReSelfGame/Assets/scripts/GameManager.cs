﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public static BlockCreator blockcreator;
    public GameObject player;
    public int startMemoryBlockPosition = 6;
    public int hardness;
    public int numBlockChangedist;
    public float realChangeDist;
    public DifficultyManager diffMan;
    public static int memPlacingProb = 50;  //the probability for replacing the mamory when frank turns
    public static int memoryPlacmentDist = 3;
    public float catchingDistance = 4;

    private static LostMemScript memory;
    private static bool isSecondPart = false;
    private static List<int> endBlocksNums;
    private static int curBlockIndx;
    private static int realEndIndex;
    private static Block endPos;
    private static int lastIndWasDarkened = 0;
    private static int curMemPosInd;

    // Use this for initialization
    void Start () {
        Cursor.visible = false;
        blockcreator = FindObjectOfType<BlockCreator>();
        memory = FindObjectOfType<LostMemScript>();
        endBlocksNums = blockcreator.init();
        realEndIndex = endBlocksNums[Random.Range(0, endBlocksNums.Count)];
        endPos = blockcreator.getBlock(realEndIndex);
        float initialHallwayBlockLen = blockcreator.getInitalHallLength();
        //Instantiate (memory, blockcreator.getBlock(startMemoryBlockPosition), 0f);
		//Debug.Log(new Vector3(startMemoryBlockPosition*initialHallwayBlockLen - (initialHallwayBlockLen/2), 0, 0));
		memory.teleport(new Vector3(startMemoryBlockPosition*initialHallwayBlockLen - (initialHallwayBlockLen/2), 0, 0));
    }
	
	// Update is called once per frame
	void Update () {
        //checking if we sould create another branch:
        if (Mathf.Abs(curBlockIndx - realEndIndex) < numBlockChangedist && Vector3.Distance(player.transform.position, endPos.transform.position) < realChangeDist) {
			endBlocksNums = blockcreator.addBranch(endPos, blockcreator.getBlock(realEndIndex).transform.forward);
            realEndIndex = endBlocksNums[Random.Range(0, endBlocksNums.Count)];
            endPos = blockcreator.getBlock(realEndIndex);
        }
        difficulty();
        if(isSecondPart){
            if (Vector3.Distance(player.transform.position, memory.transform.position) < catchingDistance) {
                memory.isInrange(true);
            } else {
                memory.isInrange(false);
            }
        }
	}

    void difficulty(){
        if (isSecondPart) {
            //TODO
        } else {
            //TODO
        }
    }

    public static void startSecondPart(){
        isSecondPart = true;
        memory.changeToSecondPartOfLevel();
    }

    //for use of block entrance triger.
    public static void setFrankBlockPos(int blockNum){
        curBlockIndx = blockNum;
        Block chosenBlock = blockcreator.getBlock(blockNum);
        if(chosenBlock == null) {
            return;
        }
        if (chosenBlock.getType() == Type.hallwayTurnL || chosenBlock.getType() == Type.hallwayTurnR) {
            if (Random.Range(0, 100) <= memPlacingProb) {
                Block b = blockcreator.getBlock(blockNum + memoryPlacmentDist);
                if (b != null && b.getType() != Type.mysteryRoom && !memory.isVisable()) {
                        memory.teleport(b.transform.position - (b.getBlockVec() / 2));
                }
            }
        }
        turnLightsOff();
    }

    public static int getFrankPos() {
        return curBlockIndx;
    }

    public static void setMemoryPos(int blockNum) {
        curMemPosInd = blockNum;
        turnLightsOff();
    }

    private static void turnLightsOff() {
        if (curMemPosInd - curBlockIndx > 5 || lastIndWasDarkened < curBlockIndx - 6)
        {
            for (; lastIndWasDarkened < curMemPosInd - 8 && lastIndWasDarkened <= curBlockIndx; lastIndWasDarkened++)
            {
                Block toDarken = blockcreator.getBlock(lastIndWasDarkened);
                if (toDarken != null)
                {
                    toDarken.turnOffLihgts();
                }
            }
            if(curMemPosInd - curBlockIndx > 10) {
                memory.teleport(memory.transform.position); //make it stop running
            }
        }
    }

    public static void theEndOfLevel(){
        //
    }


}
