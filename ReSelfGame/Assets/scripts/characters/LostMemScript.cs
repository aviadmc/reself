﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//[RequireComponent(typeof(Rigidbody))]
public class LostMemScript : MonoBehaviour {
    
    public float speed = 12;
    public float secondPartSpeed = 10;
    public Camera mainCamera;
    //public Vector3 pos;

    private Rigidbody memRigidbody;
    private List<Vector3> positions = new List<Vector3>();
    private int posIndex;
    private bool isInFirstPart = true;
    private bool shouldWait = true;
    private float camRayDistFromCenter = 0.3f;
    private int myBlockIndex = -1;
    private Animator anim;

    // Use this for initialization
    void Start () {
        memRigidbody = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();

        anim.SetBool("isWaiting", true);
    }
	
	// Update is called once per frame
    void Update () {
        if (shouldWait) {
            if (isVisable()) {
                shouldWait = false;
                anim.SetBool("isWaiting", false);
            }
        }
        goToNextPoint();
	}

    public void setPositions(List<Vector3> nextPositions, int blockIndex){
        if(blockIndex > myBlockIndex) {
            positions = nextPositions;
            posIndex = 0;
            myBlockIndex = blockIndex;
        }
    }

    //for the use of the GameManager. From now - memory is catchable.
    public void changeToSecondPartOfLevel()
    {
        speed = secondPartSpeed;     //we want frank to be able to catch it
        isInFirstPart = false;
    }

    public void teleport(Vector3 targetPos/**, List<Vector3> nextPositions*/){
        myBlockIndex = -1;
        transform.position = targetPos;
        //setPositions(nextPositions);
        shouldWait = true;
        anim.SetBool("isWaiting", true);
    }

    public void beingCalled(){
        StartCoroutine(wait());
    }

    public void isInrange(bool isIn){
        //make it fast or slow, graduatly
        if (!isInFirstPart) {
            if (isIn) {
                speed = Mathf.SmoothStep(speed, 0, 0.3f);
            } else {
                speed = Mathf.SmoothStep(speed, secondPartSpeed, 0.3f);
            }
        }
    }

    IEnumerator wait(){
        float regSpeed = speed;
        speed = speed / 2;
        yield return new WaitForSeconds(0.5f);
        speed = speed * 2.3f;
        yield return new WaitForSeconds(4);
        speed = regSpeed;
    }

    private void goToNextPoint(){
        if (posIndex < positions.Count && !shouldWait) {
            Vector3 angleTarget = positions[posIndex] - transform.position;
            //turning:
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(new Vector3(angleTarget.x, 0.0f, angleTarget.z)), Time.deltaTime * speed);
            //transform.rotation = Quaternion.LookRotation(new Vector3(angleTarget.x, 0.0f, angleTarget.z));

            //walking:
            transform.Translate(angleTarget.normalized * Time.deltaTime * speed, Space.World);

            if (Vector3.Distance(transform.position, positions[posIndex]) < 0.2f) {
                posIndex++;
            }
        }
    }

    public bool isVisable(){
        //TODO create rays (2? 4?), check if can be seen by camera.
        //return isVisible;
        float maxDist = Vector3.Distance(transform.position, mainCamera.transform.position);

        RaycastHit hit;
        Vector3 rayDIr = mainCamera.transform.position - transform.position;
        rayDIr = rayDIr / rayDIr.magnitude;
        if (Physics.Raycast(transform.position, rayDIr, out hit, maxDist)) {
            if (hit.collider.gameObject.CompareTag("Player")) {
                return true;
            }
            return false;
        }

        RaycastHit hitsR;
        RaycastHit hitsL;

        Vector3 rayRDIr = (mainCamera.transform.position + (transform.right * camRayDistFromCenter)) - transform.position;
        rayRDIr = rayRDIr / rayRDIr.magnitude;
        if (Physics.Raycast(transform.position, rayRDIr, out hitsR, maxDist)) {
            if (hitsR.collider.gameObject.CompareTag("Player")) {
                return true;
            }
            return false;
        }
        Vector3 rayLDIr = (mainCamera.transform.position - (transform.right * camRayDistFromCenter)) - transform.position;
        rayLDIr = rayLDIr / rayLDIr.magnitude;
        if (Physics.Raycast(transform.position, rayLDIr, out hitsL, maxDist)) {
            if (hitsL.collider.gameObject.CompareTag("Player")) {
                return true;
            }
            return false;
        }

        return true;
    }

    /*void OnBecameVisible(){
        isVisible = true;
    }

    void OnBecameInvisible (){
        isVisible = false;
    }*/

    public void OnTriggerEnter(Collider other) {
        if(other.tag.CompareTo("Player") == 0) {
            StartCoroutine(endLevel());
        }
    }

    IEnumerator endLevel() {
        Time.timeScale = 0.0f;
        float pauseEndTime = Time.realtimeSinceStartup + 5;
        mainCamera.GetComponent<CameraMovement>().endGame = true;
        while (Time.realtimeSinceStartup < pauseEndTime)
        {
            yield return 0;
        }
        //Application.LoadLevel("terminal_memory");
        Application.Quit();
    }
}
