﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class FrankScript : MonoBehaviour {
    
    public float walkSpeed = 8;
    public float runSpeed = 12;
    public bool canRun = true;
    public bool canCallFigure = false;
    public bool canShoot = false;
    public float turnSpeed = 3f;

    [Header("Controll keys:")]
    public KeyCode moveForwardKey = KeyCode.W;
    public KeyCode moverightKey = KeyCode.D;
    public KeyCode moveLeftKey = KeyCode.A;
    public KeyCode moveBackKey = KeyCode.S;
    public KeyCode jumpKey = KeyCode.Space;
    public KeyCode stopRunningKey = KeyCode.LeftShift;
    public KeyCode callForStopKey = KeyCode.Mouse0;
    public KeyCode shootingKey = KeyCode.Mouse1;

    private LostMemScript figure;
    private Rigidbody frankRigidbody;
    private float backWalkSlow = 0.6f;
    private float jumpPower = 30;
    private bool currentJumping = false;
    private Vector3 currentMovingDirection = new Vector3();
    private Animator animator;
    private ReflectionProbe reflet;
    private AudioSource steps;

    //movement bools (also for animation):
    private bool is_forward;
    private bool is_right;
    private bool is_left;
    private bool is_back;
    private bool is_jump;
    private bool is_running;
    private bool is_callingFigure;
    private bool is_Shooting;

	// Use this for initialization
	void Start () {
        frankRigidbody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        figure = FindObjectsOfType<LostMemScript>()[0];
        reflet = GetComponent<ReflectionProbe>();
        steps = GetComponent<AudioSource>();
    }
	
    // movement must be in FixedUpdate 
    void FixedUpdate () {
        animator.SetBool("isRunning", false);

        getMovementBools();
        turn();
        move();
        animate();
	}

    private void getMovementBools(){
        is_forward = Input.GetKey(moveForwardKey);
        is_right = Input.GetKey(moverightKey);
        is_left = Input.GetKey(moveLeftKey);
        is_back = Input.GetKey(moveBackKey);
        is_jump = Input.GetKey(jumpKey);
        is_running = (!Input.GetKey(stopRunningKey)) && canRun;
        is_callingFigure = Input.GetKey(callForStopKey) && canCallFigure;
        is_Shooting = Input.GetKey(shootingKey) && canShoot;
    }

    private void move(){
        var curSpeed = Input.GetKey(stopRunningKey) ? walkSpeed : runSpeed;

        if (currentJumping && frankRigidbody.velocity.y < 0.002f && frankRigidbody.velocity.y > -0.002f) {
            currentJumping = false;
        }
        if (!currentJumping) {
            if (is_jump) {
                currentJumping = true;
                var jumpDir = transform.TransformDirection(currentMovingDirection);
                frankRigidbody.AddForce((jumpDir * 5 + Vector3.up) * jumpPower * curSpeed);
            }
            if (is_callingFigure) {
                StartCoroutine(callFigure());
            }

            currentMovingDirection = new Vector3();
            if (is_forward) {
                currentMovingDirection += Vector3.forward * curSpeed * Time.deltaTime;
                steps.mute = false;
            }
            else {
                steps.mute = true;
            }
            if (is_right) {
                currentMovingDirection += Vector3.right * curSpeed * Time.deltaTime;
            }
            if (is_left) {
                currentMovingDirection -= Vector3.right * curSpeed * Time.deltaTime;
            }
            if (is_back) {
                currentMovingDirection -= Vector3.forward * walkSpeed * backWalkSlow * Time.deltaTime;
            }
            transform.Translate(currentMovingDirection);
        }

    }

    private void turn(){
        transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);
        transform.Rotate(0, Input.GetAxis("Mouse X") * turnSpeed, 0);
    }

    IEnumerator callFigure(){
        canCallFigure = false;
        yield return new WaitForSeconds(0.5f);
        figure.beingCalled();

        yield return new WaitForSeconds(10);
        canCallFigure = true;
    }

    private void animate(){
        animator.SetBool("isRunning", is_running && (is_forward || is_back) && !is_jump );
    }
}
