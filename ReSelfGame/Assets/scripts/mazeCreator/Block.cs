﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public enum Type{
	hallwayStright,
	hallwayTurnR,
	hallwayTurnL,
	//hallwayJunction,
	mysteryRoom,
	stairCaseStright,
	//stairCaseTurnR,
	//stairCaseTurnL,
	//deadEndStright,
	//deadEndL,
	//deadEndR,
	//MeToDeadEnd,
	//MeToDeadStart,
	//MeToStairCaseTurnR,
	//MeToStairCaseTurnL,
	Tjunction,
	//Xjunction,
	empty,
}
public enum Orientation{
	up,
	down,
	right,
	left
}

public class Block : MonoBehaviour {
	public int map_x, map_y;
	public List<Block> canFollowMe;
	public List<object> obsticles;
	public List<object> obsticlesPlacements;
	public List<object> obsticlesProbabilities;
	public List<float> BlockProbabilities;
	private Vector3 blockVec;
	public Type type;
	public Block turnMeToDeadEnd, turnMeToDeadStart;

	Orientation orientation;
	Vector2 position;
	int floorNum;
	public int blockIndex;
	public float length, hight, width;

    //can we do it not public?
    public GameObject walkWay;
	public GameObject obstacles;

    private Light[] lights;
    private Transform[] allBlockStuff;
    private List<MeshRenderer> mrList;
    private BlockCreator blockCreator;
    private bool blocking;      //sould we block this way

	public void setPosition(int x, int y){
		this.position.x = x;
		this.position.y = y;
	}
	public Orientation getOrientation(){
		return orientation;
	}
	public Vector3 getBlockVec()
	{
		return blockVec;
	}
	public void setBlockVec(Vector3 vec)
	{
		blockVec = vec;
	}
	/*public void setOrientation(Orientation orientation){
		this.orientation = orientation;
	}*/
	public Type getSquareType(){
		return type;
	}
	public void setType(Type type){
		this.type = type;
	}
	public Type getType(){
		return this.type;
	}
	//return the 2d map position
	public Vector2 getSquarePos(){
		return position;
	}
	public int getSquareFloor(){
		return floorNum;
	}
	public void setFloor(int floor){
		this.floorNum = floor;
	}
	public void setXY (int x, int y)
	{
		this.map_x = x;
		this.map_y = y;
	}
	// Use this for initialization
	void Start () {
        lights = GetComponentsInChildren<Light>();
        allBlockStuff = GetComponentsInChildren<Transform>();
        mrList = new List<MeshRenderer>(GetComponentsInChildren<MeshRenderer>());
        blocking = false;
    }

	public void blockInit(DifficultyManager dm, BlockCreator bc){
		Transform[] obs = obstacles.GetComponentsInChildren<Transform> ().Where(ob => ob.parent.gameObject == obstacles.gameObject).ToArray();
		int ind = 10;	//no more than 9 optional obs in a block
		if (dm.getObstacleProb () > Random.Range(0.0f, 1.0f)) {
			int size = obs.Length;
			if (size > 0) {
				ind = Random.Range (0, size);
			}
		}
		int i = 0;
		foreach (Transform ob in obs) {
			if (i != ind) {
				ob.gameObject.SetActive (false);
			}
			i++;
		}

        blockCreator = bc;
	}
	
	// Update is called once per frame
	void Update () {
        if(blocking && GameManager.getFrankPos() > blockIndex) {
            BoxCollider bc = gameObject.AddComponent<BoxCollider>();
            bc.center = new Vector3(0, 3, 0);
            bc.size = new Vector3(16, 6, 1);

            blockCreator.DeleteBlock(blockIndex - 1);
            //if (prevBlock != null) {
            //    Destroy(prevBlock.gameObject);      //TODO: delete it through blockCreator
            //}

            blocking = false;
        }
	}

    void OnTriggerEnter(Collider other){
        LostMemScript mem = other.GetComponent<LostMemScript>();
		if (mem != null) {
			List<Vector3> posList = new List<Vector3> ();
			Transform[] ts = walkWay.GetComponentsInChildren<Transform> ();
			for (int i = 1; i < ts.Length; i++) {
				posList.Add (ts [i].position);
			}
			Debug.Log ("posList: " + posList.ToArray()[0] + " index: " + blockIndex);
			mem.setPositions (posList, blockIndex);
            GameManager.setMemoryPos(blockIndex);
        } else if(other.GetComponent<FrankScript>() != null) {
			GameManager.setFrankBlockPos (blockIndex);

		}
    }

    public void turnOffLihgts() {
        foreach(Transform trans in allBlockStuff) {
            trans.gameObject.layer = 10;
        }
        foreach(Light light in lights) {
            light.transform.parent.gameObject.SetActive(false);
        }
        foreach(MeshRenderer mr in mrList) {
            mr.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;
        }
        blocking = true;
    }
}
