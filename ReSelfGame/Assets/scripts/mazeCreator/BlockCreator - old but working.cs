﻿/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapBlock
{
	bool isAlive;
	int index;
	int floor = 0;
	public void setAlive(bool b)
	{
		this.isAlive = b;
	}
	public void setIndex(int idx)
	{
		this.index = idx;
	}
	public void setfloor(int f)
	{
		this.floor = f;
	}
 public int getfloor()
 {
  return this.floor;
 }
}


public class BlockCreator : MonoBehaviour
{

    ////////////////////////////////////////////
    public List<Block> mysteryRooms;
    public Block firstRoom;
    public int blocksPerBranch;
    public int MaxNumberOfMystryRooms;
    public int maxHallLength;
    int mysteryRoomsCount, strightHallInARow;
    int blockCounter;
    public int initialHallLength;
    public List<MapBlock>[,] map2d;
    public Block initialHallBlock;
    public List<Block> blocksList;
    List<Block> blocksOfMaxIDX;
    private Block branchStart, branchEnd;
    private DifficultyManager dm;
    public Block stairs;
    bool canAddJunction;
    ////////////////////////////////////////////


    ////////////////////////////////////////////

    void Start()
    {
        //init ();	
    }
    ////////////////////////////////////////////

    public Block getBlock(int index)
    {
        foreach (Block b in blocksList)
        {
            if (b.blockIndex == index)
            {
                return b;
            }
        }
        return null;
    }
    ////////////////////////////////////////////
    bool flipACoin()
    {
        int randomNum;
        randomNum = (int)Random.Range(0f, 1f);
        if (randomNum == 1)
        {
            return true;
        }
        else
            return false;
    }
    ////////////////////////////////////////////
    public float getInitalHallLength()
    {
        return initialHallLength;
    }
    ////////////////////////////////////////////	
    private Block pickNextBlock(Block currBlock)
    {
        Block nxtBlock;
        int blockPicker, randomNum;
        while (true)
        {
            blockPicker = (int)Random.Range(0f, (int)currBlock.BlockProbabilities.Count);
            if (currBlock.canFollowMe[blockPicker].getType() == Type.hallwayStright && strightHallInARow == maxHallLength)
            {
                continue;
            }

            int probability = (int)(currBlock.BlockProbabilities[blockPicker] * 100);
            randomNum = (int)Random.Range(0f, 100f);
            if (randomNum < probability)
            {
                nxtBlock = currBlock.canFollowMe[blockPicker];
                break;
            }
        }
        if (nxtBlock.getType() == Type.hallwayStright)
        {
            strightHallInARow += 1;
        }
        else
        {
            strightHallInARow = 0;
        }
        return nxtBlock;
    }
    ////////////////////////////////////////////	
    public List<int> init()
    {
        dm = FindObjectOfType<DifficultyManager>();
        /* variables declaration*/
        /**********************************/
 /*       blocksOfMaxIDX = new List<Block>();  //the list of the current blocks with max idx to branch from
                                             //bool junction = false;
        map2d = new List<MapBlock>[4 * 2 * (blocksPerBranch + initialHallLength), 4 * 2 * (blocksPerBranch + initialHallLength)];  //the 2d map
        for (int i = 0; i < 4 * 2 * (blocksPerBranch + initialHallLength); i++)
        {
            for (int j = 0; j < 4 * 2 * (blocksPerBranch + initialHallLength); j++)
            {
                map2d[i, j] = new List<MapBlock>();
            }
        }
        blockCounter = 0;
        blocksList = new System.Collections.Generic.List<Block>();
        mysteryRoomsCount = 0;
        strightHallInARow = 0;  //a counter for how much stright hall rooms in a row were put so far
        Quaternion currentAngleOfPlacing = new Quaternion();
        Quaternion junctionLeftAngleOfPlacing = new Quaternion();
        Vector3 currBlockLOcation = new Vector3(0, 0, 0);
        Vector3 nxtBlockLOcation, nxt_Block_Moving_Vec;
        Vector2 blockMapLocation; // the 3d block location in the 2d map
        nxt_Block_Moving_Vec = new Vector3(initialHallBlock.length, 0, 0);
        Block nxtBlock;
        int currFloor = 0;
        float prev_block_hight = 0;
        int nextx, nexty;
        nextx = nexty = 4 * blocksPerBranch + initialHallLength;
        /**********************************/
        /* world creation*/
        /**********************************/
/*        nxtBlockLOcation = currBlockLOcation;
        Block currBlock = Instantiate(firstRoom, currBlockLOcation, Quaternion.Euler(0,-90,0));
        currBlock.setXY(4 * blocksPerBranch + initialHallLength, 4 * blocksPerBranch + initialHallLength);
        currBlock.blockIndex = 0;
        currBlock.blockInit(dm);
        currBlock.blockIndex = blockCounter;

        MapBlock currMapBlock = new MapBlock();
        //Vector2 nxtnxtBlock2dMapLoc = new Vector3 (0, 0, 0);
        map2d[(4 * blocksPerBranch + initialHallLength), (4 * blocksPerBranch + initialHallLength)].Add(currMapBlock);
        currMapBlock.setAlive(true);
        currMapBlock.setIndex(blockCounter);
        currMapBlock.setfloor(currFloor);

        blockMapLocation = new Vector2(4 * blocksPerBranch + initialHallLength, 4 * blocksPerBranch + initialHallLength);

        /**********************************/
        //place initial hall
        /**********************************/
/*        for (int i = 0; i < initialHallLength; i++)
        {
            nxtBlockLOcation += nxt_Block_Moving_Vec;
            currBlock = Instantiate(initialHallBlock, nxtBlockLOcation, Quaternion.Euler(0, -90, 0));
            currBlock.blockInit(dm);
            currBlock.setBlockVec(nxt_Block_Moving_Vec);
            blocksList.Add(currBlock);
            currBlockLOcation = nxtBlockLOcation;
            blockCounter += 1;
            currBlock.blockIndex = blockCounter;
            blockMapLocation.x += nxt_Block_Moving_Vec.x / currBlock.length;
            blockMapLocation.y += nxt_Block_Moving_Vec.y / currBlock.length;
            currBlock.setXY((int)blockMapLocation.x, (int)blockMapLocation.y);
            currMapBlock = new MapBlock();
            map2d[(int)blockMapLocation.x, (int)blockMapLocation.y].Add(currMapBlock);
            currMapBlock.setAlive(true);
            currMapBlock.setfloor(currFloor);
            currMapBlock.setIndex(blockCounter);
        }
        /**********************************/
        //place rest of the blocks
        /**********************************/
/*        for (int i = 0; i < blocksPerBranch; i++)
        {
            if (currBlock.getType() == Type.hallwayTurnR)
            {
                currentAngleOfPlacing.eulerAngles += new Vector3(0, 90, 0);
                nxt_Block_Moving_Vec = Quaternion.Euler(0, 90, 0) * nxt_Block_Moving_Vec;
            }
            else if (currBlock.getType() == Type.hallwayTurnL)
            {
                currentAngleOfPlacing.eulerAngles -= new Vector3(0, 90, 0);
                nxt_Block_Moving_Vec = Quaternion.Euler(0, -90, 0) * nxt_Block_Moving_Vec;
            }

            else if (currBlock.getType() == Type.mysteryRoom)
            {
                mysteryRoomsCount++;
            }
            else if (currBlock.getType() == Type.Tjunction)
            {
                currentAngleOfPlacing.eulerAngles += new Vector3(0, 90, 0);
            }

            nxtBlockLOcation += nxt_Block_Moving_Vec;
            nxtBlock = Instantiate(pickNextBlock(currBlock), nxtBlockLOcation, Quaternion.Euler(0, -90, 0));
            nxtBlock.blockInit(dm);

            //sets the 2d map coords for the blocks, and sets them in the 2d map as well
            /**********************************/
  /*          nextx = Mathf.RoundToInt(nxt_Block_Moving_Vec.x / currBlock.length);
            nexty = Mathf.RoundToInt(nxt_Block_Moving_Vec.z / currBlock.length);
            nextx += currBlock.map_x;
            nexty += currBlock.map_y;

            nxtBlock.setXY(nextx, nexty);

            //if the picked block is a junction but we have too many junctions pick another

            if (((nxtBlock.getType() == Type.Tjunction) && canAddJunction == false))
            {
                while ((currBlock.getType() != Type.Tjunction))
                {
                    nxtBlock = pickNextBlock(currBlock);
                }
            }

            //add next block as i=the chosen block or a deadend
            /**********************************/
     /*       if (i == blocksPerBranch - 1)
            {
                Destroy(nxtBlock.gameObject);
                currBlock = Instantiate(nxtBlock.turnMeToDeadEnd, nxtBlockLOcation, Quaternion.Euler(0, -90, 0));
                currBlock.blockInit(dm);
                branchEnd = currBlock;
            }
            else
            {
                if ((map2d[(nxtBlock.map_x), (nxtBlock.map_y)].Count != 0) && (map2d[(nxtBlock.map_x), (nxtBlock.map_y)])[map2d[(nxtBlock.map_x), (nxtBlock.map_y)].Count - 1].getfloor() == currFloor)
                {
                    currFloor++;
                    prev_block_hight = currBlock.hight;
                    Block tempCurr = Instantiate(stairs, currBlockLOcation, Quaternion.Euler(0, -90, 0));
                    tempCurr.blockInit(dm);
                    tempCurr.setXY(currBlock.map_x, currBlock.map_y);
                    Destroy(currBlock.gameObject);
                    currBlock = tempCurr;
                    currBlock.blockIndex = blockCounter;
                    nxtBlockLOcation += (new Vector3(0, prev_block_hight, 0));
                    nxtBlock.transform.position = nxtBlockLOcation;
                    currBlock = nxtBlock;
                }
                else
                {
                    currBlock = nxtBlock;
                    if (nxtBlock.getType() == Type.Tjunction)
                    {
                        Block junctionBlockLeft = pickNextBlock(currBlock);
                        junctionBlockLeft = Instantiate(junctionBlockLeft, nxt_Block_Moving_Vec - (new Vector3(90, 0, 0)), Quaternion.Euler(0, -90, 0));
                        junctionBlockLeft.blockInit(dm);
                        junctionBlockLeft.blockIndex = blockCounter + 1;
                        int nextxBranch = Mathf.RoundToInt((nxt_Block_Moving_Vec.x) / (currBlock.length));
                        int nextyBranch = Mathf.RoundToInt((nxt_Block_Moving_Vec.z) / (currBlock.length));
                        nextxBranch += currBlock.map_x;
                        nextyBranch += currBlock.map_y;
                        junctionBlockLeft.setXY(nextxBranch, nextyBranch);
                        junctionBlockLeft.setFloor(currFloor);
                        MapBlock mapblock = new MapBlock();
                        mapblock.setAlive(true);
                        mapblock.setfloor(currFloor);
                        mapblock.setIndex(blockCounter + 1);
                        map2d[junctionBlockLeft.map_x, junctionBlockLeft.map_y].Add(mapblock);

                        junctionBlockLeft.transform.Rotate(junctionLeftAngleOfPlacing.eulerAngles - (new Vector3(90, 0, 0)));
                        addBranchHelper(junctionBlockLeft, nxt_Block_Moving_Vec - (new Vector3(90, 0, 0)));
                        canAddJunction = false;
                    }
                }
            }
            /**********************************/
   /*         currBlock.setFloor(currFloor);
            currBlock.setBlockVec(nxt_Block_Moving_Vec);
            blocksList.Add(currBlock);

            if (i == blocksPerBranch - 1)
            {
                blocksOfMaxIDX.Add(currBlock);
            }
            if (currBlock.getType() == Type.mysteryRoom)
            {
                mysteryRooms.Add(currBlock);
            }
            currBlock.transform.Rotate(currentAngleOfPlacing.eulerAngles);
            currBlockLOcation = nxtBlockLOcation;
            blockCounter += 1;

            currBlock.blockIndex = blockCounter;
            //sets the block in the 2d map

            blockMapLocation.x += nxt_Block_Moving_Vec.x / currBlock.length;
            blockMapLocation.y += nxt_Block_Moving_Vec.z / currBlock.length;
            currMapBlock = new MapBlock();
            currMapBlock.setAlive(true);
            currMapBlock.setIndex(blockCounter);
            currMapBlock.setfloor(currFloor);
            map2d[currBlock.map_x, currBlock.map_y].Add(currMapBlock);
        }
        /**********************************/

   /*     List<int> l = new List<int>();
        foreach (Block b in blocksOfMaxIDX)
        {
            l.Add(b.blockIndex);
        }
        return l;
    }
    ////////////////////////////////////////////
    void eraseStem()
    {

    }
    ////////////////////////////////////////////
    void moveBranchToCenter()
    {

    }
    ////////////////////////////////////////////
    public List<int> addBranch(Block frankPosition, Vector3 branchDirection)
    {
        branchStart = branchEnd;
        Block currBlock = Instantiate(branchStart.turnMeToDeadStart, branchStart.transform.position, Quaternion.Euler(0, -90, 0));
        currBlock.blockInit(dm);
        List<int> l = new List<int>();
        foreach (Block b in blocksOfMaxIDX)
        {
            l.Add(b.blockIndex);
        }
        return l;
    }
    ////////////////////////////////////////////

    public List<Block> addBranchHelper(Block frankPosition, Vector3 branchDirection)
    {
        /* variables declaration*/
        /**********************************/
   /*     strightHallInARow = 0;        //a counter for how much stright hall rooms in a row were put so far		
        Quaternion currentAngleOfPlacing = new Quaternion();
        Quaternion junctionLeftAngleOfPlacing = new Quaternion();
        Vector3 currBlockLOcation = new Vector3(0, 0, 0);
        Vector3 nxtBlockLOcation, nxt_Block_Moving_Vec;
        Vector2 blockMapLocation; // the 3d block location in the 2d map		
        nxt_Block_Moving_Vec = new Vector3(initialHallBlock.length, 0, 0);
        Block nxtBlock;
        int currFloor = 0;
        float prev_block_hight = 0;
        int prevx, prevy, nextx, nexty;
        prevx = prevy = nextx = nexty = 4 * blocksPerBranch + initialHallLength;
        //Vector3 nxt_Block_Moving_Vec_TEMP;		
        /**********************************/
        /* world creation*/
        /**********************************/
 /*       nxtBlockLOcation = currBlockLOcation;
        Block currBlock = Instantiate(firstRoom, currBlockLOcation, currentAngleOfPlacing);
        currBlock.setXY(4 * blocksPerBranch + initialHallLength, 4 * blocksPerBranch + initialHallLength);
        currBlock.blockIndex = 0;
        //blockCounter++;		
        currBlock.blockIndex = blockCounter;
        //print ("map coords : "+(blocksPerBranch / 2) + "," + (blocksPerBranch / 2) );		
        MapBlock currMapBlock = new MapBlock();
        //Vector2 nxtnxtBlock2dMapLoc = new Vector3 (0, 0, 0);		
        /**********************************/
        //place rest of the blocks		
        /**********************************/
 /*       for (int i = 0; i < blocksPerBranch; i++)
        {
            if (currBlock.getType() == Type.hallwayTurnR)
            {
                currentAngleOfPlacing.eulerAngles += new Vector3(0, 90, 0);
                nxt_Block_Moving_Vec = Quaternion.Euler(0, 90, 0) * nxt_Block_Moving_Vec;
            }
            else if (currBlock.getType() == Type.hallwayTurnL)
            {
                currentAngleOfPlacing.eulerAngles -= new Vector3(0, 90, 0);
                nxt_Block_Moving_Vec = Quaternion.Euler(0, -90, 0) * nxt_Block_Moving_Vec;
            }
            else if (currBlock.getType() == Type.mysteryRoom)
            {
                mysteryRoomsCount++;
            }
            else if (currBlock.getType() == Type.Tjunction)
            {
                currentAngleOfPlacing.eulerAngles += new Vector3(0, 90, 0);
            }
            nxtBlockLOcation += nxt_Block_Moving_Vec;
            nxtBlock = Instantiate(pickNextBlock(currBlock), nxtBlockLOcation, Quaternion.Euler(0, -90, 0));
            //sets the 2d map coords for the blocks, and sets them in the 2d map as well		
            /**********************************/
  /*          nextx = Mathf.RoundToInt((nxt_Block_Moving_Vec.x) / (currBlock.length));
            nexty = Mathf.RoundToInt((nxt_Block_Moving_Vec.z) / (currBlock.length));
            nextx += (int)currBlock.map_x;
            nexty += (int)currBlock.map_y;
            nxtBlock.setXY(nextx, nexty);
            ////////////////////////////////////////////////////////		
            //if the picked block is a junction but we have too many junctions pick another		
            if (((nxtBlock.getType() == Type.Tjunction) && canAddJunction == false))
            {
                while ((currBlock.getType() != Type.Tjunction))
                {
                    nxtBlock = pickNextBlock(currBlock);
                }
            }
            //add next block as i=the chosen block or a deadend		
            /**********************************/
  /*          if (i == blocksPerBranch - 1)
            {
                Destroy(nxtBlock.gameObject);
                currBlock = Instantiate(nxtBlock.turnMeToDeadEnd, nxtBlockLOcation, Quaternion.Euler(0, -90, 0));
                branchEnd = currBlock;
            }
            else
            {
                //checks for floor update		
                if ((map2d[(nxtBlock.map_x), (nxtBlock.map_y)].Count != 0) && (map2d[(nxtBlock.map_x), (nxtBlock.map_y)])[map2d[(nxtBlock.map_x), (nxtBlock.map_y)].Count - 1].getfloor()
                    == currFloor)
                {
                    currFloor++;
                    prev_block_hight = currBlock.hight;
                    Block tempCurr = Instantiate(stairs, currBlockLOcation, Quaternion.Euler(0, -90, 0));
                    tempCurr.setXY(currBlock.map_x, currBlock.map_y);
                    Destroy(currBlock.gameObject);
                    currBlock = tempCurr;
                    currBlock.blockIndex = blockCounter;
                    //Instantiate (currBlock, currBlockLOcation, Quaternion.identity);		
                    nxtBlockLOcation += (new Vector3(0, prev_block_hight, 0));
                    nxtBlock.transform.position = nxtBlockLOcation;
                    currBlock = nxtBlock;
                }
                // no floor update		
                else
                {
                    currBlock = nxtBlock;
                    if (nxtBlock.getType() == Type.Tjunction)
                    {
                        Block junctionBlockLeft = pickNextBlock(currBlock);
                        junctionBlockLeft = Instantiate(junctionBlockLeft,
                            nxt_Block_Moving_Vec - (new Vector3(90, 0, 0)), Quaternion.Euler(0, -90, 0));
                        junctionBlockLeft.blockIndex = blockCounter + 1;
                        int nextxBranch = Mathf.RoundToInt((nxt_Block_Moving_Vec.x) / (currBlock.length));
                        int nextyBranch = Mathf.RoundToInt((nxt_Block_Moving_Vec.z) / (currBlock.length));
                        nextxBranch += (int)currBlock.map_x;
                        nextyBranch += (int)currBlock.map_y;
                        junctionBlockLeft.setXY(nextxBranch, nextyBranch);
                        junctionBlockLeft.setFloor(currFloor);
                        MapBlock mapblock = new MapBlock();
                        mapblock.setAlive(true);
                        mapblock.setfloor(currFloor);
                        mapblock.setIndex(blockCounter + 1);
                        map2d[junctionBlockLeft.map_x, junctionBlockLeft.map_y].Add(mapblock);
                        junctionBlockLeft.transform.Rotate(junctionLeftAngleOfPlacing.eulerAngles - (new Vector3(90, 0, 0)));
                        addBranchHelper(junctionBlockLeft, nxt_Block_Moving_Vec - (new Vector3(90, 0, 0)));
                        canAddJunction = false;
                    }
                }
            }
            /**********************************/
  /*          currBlock.setFloor(currFloor);
            currBlock.setBlockVec(nxt_Block_Moving_Vec);
            blocksList.Add(currBlock);
            if (i == blocksPerBranch - 1)
            {
                blocksOfMaxIDX.Add(currBlock);
            }
            if (currBlock.getType() == Type.mysteryRoom)
            {
                mysteryRooms.Add(currBlock);
            }
            currBlock.transform.Rotate(currentAngleOfPlacing.eulerAngles);
            currBlockLOcation = nxtBlockLOcation;
            blockCounter += 1;
            currBlock.blockIndex = blockCounter;
            //sets the block in the 2d map		
            currMapBlock = new MapBlock();
            currMapBlock.setAlive(true);
            currMapBlock.setIndex(blockCounter);
            currMapBlock.setfloor(currFloor);
            map2d[currBlock.map_x, currBlock.map_y].Add(currMapBlock);
            currMapBlock.setAlive(true);
            currMapBlock.setIndex(blockCounter);
        }
        return blocksOfMaxIDX;

    }
}
*/