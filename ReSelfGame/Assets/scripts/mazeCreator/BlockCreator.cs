﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 


public class MapBlock
{
	bool isAlive;
	int index;
	int floor = 0;
	public void setAlive(bool b)
	{
		this.isAlive = b;
	}
	public void setIndex(int idx)
	{
		this.index = idx;
    }
    public int getIndex()
    {
        return this.index;
    }
    public void setfloor(int f)
	{
		this.floor = f;
	}
	public int getfloor()
	{
		return this.floor;
	}
}


public class BlockCreator : MonoBehaviour {
	
	////////////////////////////////////////////
	public List<Block> mysteryRooms;
	public Block firstRoom;
	public int blocksPerBranch;
	public int MaxNumberOfMystryRooms;
	public int maxHallLength;
	int mysteryRoomsCount, strightHallInARow;
	int blockCounter;
	public int initialHallLength;
	public List<MapBlock>  [,] map2d; 
	public Block initialHallBlock;
	public List<Block> blocksList;
	List<Block> blocksOfMaxIDX;
	private Block branchStart, branchEnd;
	private DifficultyManager dm;
	public Block stairs;
	bool canAddJunction;
	int currFloor;
    int mapDim;

    private int lastWasDeletedInd = -1;
    ////////////////////////////////////////////
    //-----------------------------------------------------------| Aviad add: |-----------------------------------------------------
    
    Quaternion currentAngleOfPlacing = new Quaternion ();
    Quaternion prevAngleOfPlacing = new Quaternion ();
    
    Vector3 currBlockLOcation;
    Vector3 nxtBlockLOcation, nxt_Block_Moving_Vec;
    Vector2 blockMapLocation = new Vector2(); // the 3d block location in the 2d map

    ////////////////////////////////////////////

    void Start () 
	{
        //init ();	
        //CHANGED moved here from "init" (2 lines):
        mysteryRoomsCount = 0;
        strightHallInARow = 0;        //a counter for how much stright hall rooms in a row were put so far
    }
	////////////////////////////////////////////

	public  Block getBlock (int index)
	{
		foreach (Block b in blocksList)
		{
			if(b.blockIndex == index)
			{
				return b;
			}
		}
		return null;
	}

    public void DeleteBlock(int blockId) {
        for (int i = lastWasDeletedInd + 1; i <= blockId; i++)
        {
            Block toDelete = getBlock(i);
            blocksList.Remove(toDelete);
            //map2d[toDelete.map_x, toDelete.map_y].Remove(toDelete.)       is it possible to do this?

            if (toDelete != null)
            {
                if (map2d[toDelete.map_x, toDelete.map_y] != null)
                {
                    foreach (MapBlock mb in map2d[toDelete.map_x, toDelete.map_y])
                    {
                        if (mb.getIndex() == toDelete.blockIndex)
                        {
                            map2d[toDelete.map_x, toDelete.map_y].Remove(mb);
                            break;
                        }
                    }
                }
                Destroy(toDelete.gameObject);
            }
        }
        lastWasDeletedInd = blockId;
    }

	////////////////////////////////////////////
	bool flipACoin()
	{	
		int randomNum;
		randomNum = (int)Random.Range(0f,1f);
		if (randomNum == 1) {
			return true;
		} else
			return false;
	}
	////////////////////////////////////////////
	public float getInitalHallLength()
	{
		return initialHallLength;
	}
	////////////////////////////////////////////	
	private Block pickNextBlock(Block currBlock)
	{
		Block nxtBlock;
		int blockPicker, randomNum;
		while (true) 
		{
			blockPicker = Random.Range(0, currBlock.BlockProbabilities.Count);
			if (currBlock.canFollowMe [blockPicker].getType () == Type.hallwayStright && strightHallInARow == maxHallLength 
				&& currBlock.getType() != Type.Tjunction)
			{
				continue;
			}

			int probability = (int)(currBlock.BlockProbabilities [blockPicker] * 100);
			randomNum = (int)Random.Range(0f,100f);
			if (randomNum < probability) 
			{
				nxtBlock = currBlock.canFollowMe[blockPicker];
				break;
			}
		}
		if (nxtBlock.getType() == Type.hallwayStright) 
		{
			strightHallInARow += 1;
		}
		else
		{
			strightHallInARow = 0;
		}
		return nxtBlock;
	}
	////////////////////////////////////////////	
	public List<int> init()
	{
        mapDim = 4 * 2 * (blocksPerBranch + initialHallLength);
        dm = FindObjectOfType<DifficultyManager>();
		/* variables declaration*/
		/**********************************/
		blocksOfMaxIDX = new List<Block> ();  //the list of the current blocks with max idx to branch from
		//bool junction = false;
		map2d = new List<MapBlock> [mapDim, mapDim];  //the 2d map
		for (int i = 0; i <4*2* (blocksPerBranch + initialHallLength); i++) 
		{
			for (int j = 0; j <4*2* (blocksPerBranch + initialHallLength); j++) 
			{
				map2d [i, j] = new List<MapBlock> ();
			}
		}
		blockCounter = 0;
		blocksList = new System.Collections.Generic.List <Block> ();
		/*Quaternion*/ currentAngleOfPlacing = new Quaternion ();
		/*Quaternion*/ prevAngleOfPlacing = new Quaternion ();
		Quaternion junctionLeftAngleOfPlacing = new Quaternion ();
		/*Vector3*/ currBlockLOcation = new Vector3 (0, 0, 0);
		//Vector3 nxtBlockLOcation, nxt_Block_Moving_Vec;
		//Vector2 blockMapLocation; // the 3d block location in the 2d map

		nxt_Block_Moving_Vec = new Vector3(initialHallBlock.length,0,0);

		Block nxtBlock;
		currFloor = 0;
		float prev_block_hight = 0;
		int nextx, nexty;
		 nextx = nexty = 4*blocksPerBranch+initialHallLength;
		//Vector3 nxt_Block_Moving_Vec_TEMP;
		/**********************************/
		/* world creation*/
		/**********************************/
		nxtBlockLOcation = currBlockLOcation;
		Block currBlock = Instantiate (firstRoom, currBlockLOcation, Quaternion.Euler(0, -90, 0));
		currBlock.setXY (4*blocksPerBranch + initialHallLength, 4*blocksPerBranch + initialHallLength);
		currBlock.blockIndex = 0;
		currBlock.blockInit(dm, this);
		//blockCounter++;
		currBlock.blockIndex = blockCounter; 
		//print ("map coords : "+(blocksPerBranch / 2) + "," + (blocksPerBranch / 2) );
		MapBlock currMapBlock = new MapBlock(); 
		//Vector2 nxtnxtBlock2dMapLoc = new Vector3 (0, 0, 0);
		map2d [(int)(4*blocksPerBranch + initialHallLength), (int)(4*blocksPerBranch + initialHallLength)].Add(currMapBlock );
		//initB.setXY ((int)(blocksPerBranch / 2), (int)(blocksPerBranch / 2));
		currMapBlock.setAlive (true);
		currMapBlock.setIndex(blockCounter);
		currMapBlock.setfloor (currFloor);
		blockMapLocation = new Vector2 ((int)(4*blocksPerBranch + initialHallLength),(int)(4*blocksPerBranch + initialHallLength));
		//bool hasMoveAFloorThisIteration = false;
		canAddJunction = true;

        blocksList.Add(currBlock);
        /**********************************/
        //place initial hall
        /**********************************/
        for (int i = 0; i < initialHallLength; i++) 
		{
			nxtBlockLOcation += nxt_Block_Moving_Vec;
			currBlock = Instantiate (initialHallBlock, nxtBlockLOcation, Quaternion.Euler(0, -90, 0));
			currBlock.blockInit(dm, this);
			currBlock.setBlockVec (nxt_Block_Moving_Vec);
			blocksList.Add (currBlock);
			currBlockLOcation = nxtBlockLOcation;
			blockCounter += 1;
			currBlock.blockIndex = blockCounter;
			blockMapLocation.x += nxt_Block_Moving_Vec.x / currBlock.length;
			blockMapLocation.y += nxt_Block_Moving_Vec.y / currBlock.length;
			currBlock.setXY ((int)blockMapLocation.x,(int)blockMapLocation.y);
			currMapBlock =  new MapBlock();
			map2d[(int)blockMapLocation.x,(int) blockMapLocation.y].Add( currMapBlock);
			currMapBlock.setAlive (true);
			currMapBlock.setfloor (currFloor);
			currMapBlock.setIndex (blockCounter);
		}
		/**********************************/
		//place rest of the blocks
		/**********************************/
		for (int i = 0; i < blocksPerBranch; i++)
		{	
			prevAngleOfPlacing = currentAngleOfPlacing;

			if (currBlock.getType () == Type.hallwayTurnR) {
				currentAngleOfPlacing.eulerAngles += new Vector3 (0, 90, 0);
				nxt_Block_Moving_Vec = Quaternion.Euler(0,90,0) * nxt_Block_Moving_Vec;
			} 
			else if (currBlock.getType () == Type.hallwayTurnL) 
			{
				currentAngleOfPlacing.eulerAngles -= new Vector3 (0, 90, 0);
				nxt_Block_Moving_Vec = Quaternion.Euler(0,-90,0) * nxt_Block_Moving_Vec;
			}

			else if (currBlock.getType() == Type.mysteryRoom)
			{
				mysteryRoomsCount++;
			}
			else if (currBlock.getType() == Type.Tjunction)
			{
				currentAngleOfPlacing.eulerAngles += new Vector3 (0, 90, 0);
			}

			nxtBlockLOcation += nxt_Block_Moving_Vec;
			nxtBlock = Instantiate (pickNextBlock (currBlock), nxtBlockLOcation, Quaternion.Euler(0, -90, 0));
			nxtBlock.blockInit(dm, this);

			//sets the 2d map coords for the blocks, and sets them in the 2d map as well
			/**********************************/
			nextx = Mathf.RoundToInt((nxt_Block_Moving_Vec.x)/(currBlock.length));
			nexty = Mathf.RoundToInt((nxt_Block_Moving_Vec.z)/(currBlock.length));
			nextx += (int)currBlock.map_x;
			nexty += (int)currBlock.map_y;
			nxtBlock.setXY (nextx, nexty);
            ////////////////////////////////////////////////////////
            // I'm changing from here, Aviad: |---------------------------------------------------------------
            Block tempSwitchedBlock;

			//if we just placed a turn left block into a wall
			if(currBlock.getType() == Type.hallwayTurnL)
			{
				int nextTurnx = Mathf.RoundToInt((nxt_Block_Moving_Vec.x)/(currBlock.length));
				int nextTurny = Mathf.RoundToInt((nxt_Block_Moving_Vec.z)/(currBlock.length));
				Vector2 TurnLVec = new Vector2 (nextTurnx,nextTurny);
				TurnLVec =  Quaternion.Euler (0, -90, 0)*TurnLVec ;
				if((map2d[nxtBlock.map_x, nxtBlock.map_y].Count != 0) && map2d[nxtBlock.map_x, nxtBlock.map_y]
					[map2d[nxtBlock.map_x, nxtBlock.map_y].Count - 1].getfloor() == currFloor)
				{/*
					Block temp = pickNextBlock(currBlock);
					while (temp.getType() == Type.hallwayTurnL)         //CHANGED "!=" TO "=="
					{
						temp = pickNextBlock(currBlock);
					}
                    tempSwitchedBlock = Instantiate (temp, nxtBlockLOcation, Quaternion.Euler(0, -90, 0));      //CHANGED "nxtBlock" TO "tempSwitchedBlock"
                    tempSwitchedBlock.blockInit(dm);        //CHANGED line added
                    tempSwitchedBlock.setXY(nxtBlock.map_x, nxtBlock.map_y);        //CHANGED line added
                    Destroy (nxtBlock.gameObject);
                    nxtBlock = tempSwitchedBlock;        //CHANGED line added
                    */
                    Block temp = Instantiate(initialHallBlock, currBlock.transform.position, currBlock.transform.rotation);
                    currentAngleOfPlacing = prevAngleOfPlacing;
                    nxt_Block_Moving_Vec = currentAngleOfPlacing * new Vector3(currBlock.length, 0, 0);
                    nxtBlockLOcation = currBlockLOcation + nxt_Block_Moving_Vec;
                    nxtBlock.transform.position = nxtBlockLOcation;
                    //nxtBlock.transform.Rotate(new Vector3(0, 90, 0));
                    nxtBlock.setXY(currBlock.map_x + Mathf.RoundToInt((nxt_Block_Moving_Vec.x) / (currBlock.length)), currBlock.map_y + Mathf.RoundToInt((nxt_Block_Moving_Vec.z) / (currBlock.length)));
                    temp.setXY(currBlock.map_x, currBlock.map_y);
                    temp.blockIndex = currBlock.blockIndex;
                    Destroy(currBlock.gameObject);
                    currBlock = temp;
                    currBlock.blockInit(dm, this);
                    blockMapLocation = new Vector2(nxtBlock.map_x, nxtBlock.map_y);
                }
			}
			//if we just placed a turn right block into a wall
			if(currBlock.getType() == Type.hallwayTurnR)
			{
				int nextTurnx = Mathf.RoundToInt((nxt_Block_Moving_Vec.x)/(currBlock.length));
				int nextTurny = Mathf.RoundToInt((nxt_Block_Moving_Vec.z)/(currBlock.length));
				Vector2 TurnLVec = new Vector2 (nextTurnx,nextTurny);
				TurnLVec =  Quaternion.Euler (0, 90, 0)*TurnLVec ;
                if ((map2d[nxtBlock.map_x, nxtBlock.map_y].Count != 0) && map2d[nxtBlock.map_x, nxtBlock.map_y]
                    [map2d[nxtBlock.map_x, nxtBlock.map_y].Count - 1].getfloor() == currFloor)
                {
                    /*
                        Block temp = pickNextBlock(currBlock);
                        while (temp.getType() == Type.hallwayTurnR)         //CHANGED "!=" TO "=="
                        {
                            temp = pickNextBlock(currBlock);
                        }
                        tempSwitchedBlock = Instantiate (temp, nxtBlockLOcation, Quaternion.Euler(0, -90, 0));      //CHANGED "nxtBlock" TO "tempSwitchedBlock"
                        tempSwitchedBlock.blockInit(dm);        //CHANGED line added
                        tempSwitchedBlock.setXY(nxtBlock.map_x, nxtBlock.map_y);        //CHANGED line added
                        Destroy(nxtBlock.gameObject);
                        nxtBlock = tempSwitchedBlock;        //CHANGED line added
                        */
                    Block temp = Instantiate(initialHallBlock, currBlock.transform.position, currBlock.transform.rotation);
                    currentAngleOfPlacing = prevAngleOfPlacing;
                    nxt_Block_Moving_Vec = currentAngleOfPlacing * new Vector3(currBlock.length, 0, 0);
                    nxtBlockLOcation = currBlockLOcation + nxt_Block_Moving_Vec;
                    nxtBlock.transform.position = nxtBlockLOcation;
                    //nxtBlock.transform.Rotate(new Vector3(0, -90, 0));
                    nxtBlock.setXY(currBlock.map_x + Mathf.RoundToInt((nxt_Block_Moving_Vec.x) / (currBlock.length)), currBlock.map_y + Mathf.RoundToInt((nxt_Block_Moving_Vec.z) / (currBlock.length)));
                    temp.setXY(currBlock.map_x, currBlock.map_y);
                    temp.blockIndex = currBlock.blockIndex;
                    Destroy(currBlock.gameObject);
                    currBlock = temp;
                    currBlock.blockInit(dm, this);
                    blockMapLocation = new Vector2(nxtBlock.map_x, nxtBlock.map_y);
                }
			}

			//if the picked block is a junction but we have too many junctions pick 
			if (((nxtBlock.getType() == Type.Tjunction ) && canAddJunction == false))
			{
                Block temp = null;      //CHANGED line added
                while ((nxtBlock.getType() == Type.Tjunction))         //CHANGED "!=" TO "=="
                {
                    temp = pickNextBlock (currBlock);      //CHANGED "nxtBlock" TO "tempSwitchedBlock"
                }
                tempSwitchedBlock = Instantiate(temp, nxtBlockLOcation, Quaternion.Euler(0, -90, 0));      //CHANGED line added
                tempSwitchedBlock.blockInit(dm, this);        //CHANGED line added
                tempSwitchedBlock.setXY(nxtBlock.map_x, nxtBlock.map_y);        //CHANGED line added
                Destroy(nxtBlock.gameObject);
                nxtBlock = tempSwitchedBlock;        //CHANGED line added
            }

			//add next block as i=the chosen block or a deadend
			/**********************************/
			if (i == blocksPerBranch - 1)
			{
                currBlock = Instantiate (nxtBlock.turnMeToDeadEnd, nxtBlockLOcation, Quaternion.Euler(0, -90, 0));
                Destroy (nxtBlock.gameObject);
				currBlock.blockInit(dm, this);
				branchEnd = currBlock;
			}
			else 
			{
				//checks for floor update
				if ((map2d [(nxtBlock.map_x), (nxtBlock.map_y)].Count != 0) && (map2d [(nxtBlock.map_x), (nxtBlock.map_y)]) [map2d [(nxtBlock.map_x), (nxtBlock.map_y)].Count - 1].getfloor ()
					== currFloor) {
					currFloor++;
					prev_block_hight = currBlock.hight; 
					Block tempCurr = Instantiate (stairs, currBlockLOcation, Quaternion.Euler(0, -90, 0));
					tempCurr.blockInit(dm, this);
					tempCurr.setXY (currBlock.map_x, currBlock.map_y);
                    Destroy (currBlock.gameObject);
					currBlock = tempCurr;
					currBlock.blockIndex = blockCounter;
					//Instantiate (currBlock, currBlockLOcation, Quaternion.identity);
					nxtBlockLOcation += (new Vector3 (0, prev_block_hight, 0));
					nxtBlock.transform.position = nxtBlockLOcation;
					currBlock.transform.Rotate (prevAngleOfPlacing.eulerAngles);
					currBlock = nxtBlock;
                    //currBlock.transform.Rotate(prevAngleOfPlacing.eulerAngles);
                    currMapBlock =  new MapBlock();
					currMapBlock.setAlive (true);
					currMapBlock.setfloor (currFloor);
					currMapBlock.setIndex (blockCounter);
					map2d[(int)blockMapLocation.x,(int) blockMapLocation.y].Add(currMapBlock);

				} 
				// no floor update
				else 
				{
					currBlock = nxtBlock;
					if (nxtBlock.getType () == Type.Tjunction && canAddJunction) {
						canAddJunction = false;
						Block junctionBlockLeft = pickNextBlock (currBlock);
						while (junctionBlockLeft.getType () != Type.hallwayStright)
						{
							junctionBlockLeft = pickNextBlock (currBlock);
						}
						junctionBlockLeft = Instantiate (junctionBlockLeft,
							nxt_Block_Moving_Vec - (new Vector3 (90, 0, 0)), Quaternion.Euler(0, -90, 0));
						junctionBlockLeft.blockInit(dm, this);
						junctionBlockLeft.blockIndex = blockCounter+1;
						int nextxBranch = Mathf.RoundToInt((nxt_Block_Moving_Vec.x)/(currBlock.length));
						int nextyBranch = Mathf.RoundToInt((nxt_Block_Moving_Vec.z)/(currBlock.length));
						nextxBranch += (int)currBlock.map_x;
						nextyBranch += (int)currBlock.map_y;
						junctionBlockLeft.setXY (nextxBranch, nextyBranch);
						junctionBlockLeft.setFloor (currFloor);
						MapBlock mapblock = new MapBlock ();
						mapblock.setAlive (true);
						mapblock.setfloor (currFloor);
						mapblock.setIndex (blockCounter+1);
						map2d [junctionBlockLeft.map_x, junctionBlockLeft.map_y].Add (mapblock);
						junctionBlockLeft.transform.Rotate (junctionLeftAngleOfPlacing.eulerAngles+(new Vector3 (90, 0, 0)));
						addBranchHelper (junctionBlockLeft, nxt_Block_Moving_Vec - (new Vector3 (90, 0, 0)));

					}
				}
			}

			/**********************************/
			currBlock.setFloor (currFloor);
			currBlock.setBlockVec (nxt_Block_Moving_Vec);
			blocksList.Add (currBlock);

		
			if (i == blocksPerBranch-1)
			{
				blocksOfMaxIDX.Add (currBlock);
			}
			if (currBlock.getType() == Type.mysteryRoom)
			{
				mysteryRooms.Add (currBlock);
			}
			currBlock.transform.Rotate (currentAngleOfPlacing.eulerAngles);
 			currBlockLOcation = nxtBlockLOcation;
			blockCounter += 1;
			currBlock.blockIndex = blockCounter;
			//sets the block in the 2d map

			blockMapLocation.x += nxt_Block_Moving_Vec.x / currBlock.length;
			blockMapLocation.y += nxt_Block_Moving_Vec.z / currBlock.length;
			currMapBlock = new MapBlock ();
			currMapBlock.setAlive (true);
			currMapBlock.setIndex (blockCounter);
			currMapBlock.setfloor (currFloor);
			map2d [currBlock.map_x,currBlock.map_y].Add(currMapBlock);
			currMapBlock.setAlive (true);
			currMapBlock.setIndex(blockCounter);


		}

		/**********************************/
		List<int> l = new List<int>();
		foreach (Block b in blocksOfMaxIDX)
		{
			l.Add(b.blockIndex);
		}
		return l;
	}


	////////////////////////////////////////////
	void eraseStem()
	{
		
	}
	////////////////////////////////////////////
	void moveBranchToCenter()
	{
        int centerMap = Mathf.CeilToInt(mapDim / 2);
        List<MapBlock>[,] tempMap2d = new List<MapBlock>[mapDim, mapDim];
        for(int i = 0; i< mapDim; i++) {
            for (int j = 0; j < mapDim; j++)
            {
                tempMap2d[i, j] = new List<MapBlock>();
            }
        }

        Block last = blocksList[blocksList.Count-1];
        int moveXDim = last.map_x - centerMap;
        int moveYDim = last.map_y - centerMap;

        foreach(Block currBlock in blocksList) {
            if (tempMap2d[currBlock.map_x - moveXDim, currBlock.map_y - moveYDim] != null && tempMap2d[currBlock.map_x - moveXDim, currBlock.map_y - moveYDim].Count == 0)
            {
                tempMap2d[currBlock.map_x - moveXDim, currBlock.map_y - moveYDim] = map2d[currBlock.map_x, currBlock.map_y];
            }

            currBlock.setXY(currBlock.map_x - moveXDim, currBlock.map_y - moveYDim);
        }

        map2d = tempMap2d;
    }
	////////////////////////////////////////////
	public List<int> addBranch(Block frankPosition, Vector3 branchDirection)
	{
        moveBranchToCenter();

        branchStart = branchEnd;
        //Block currBlock = Instantiate (branchStart.turnMeToDeadStart, branchStart.transform.position ,  Quaternion.Euler(0, -90, 0));
		//currBlock.blockInit(dm, this);
		canAddJunction = true;

		
		return addBranchHelper(frankPosition, branchDirection);
		 

	}

	public List<int> addBranchHelper(Block frankPosition, Vector3 branchDirection)
	{
        /* variables declaration*/
        /**********************************/
        blocksOfMaxIDX.Clear();
        strightHallInARow = 0;        //a counter for how much stright hall rooms in a row were put so far
		//Quaternion currentAngleOfPlacing = new Quaternion ();
		//Quaternion prevAngleOfPlacing = new Quaternion ();
		Quaternion junctionLeftAngleOfPlacing = new Quaternion ();
		//Vector3 currBlockLOcation = frankPosition.transform.position;
		//Vector3 nxtBlockLOcation, nxt_Block_Moving_Vec;
		//Vector2 blockMapLocation = new Vector2(); // the 3d block location in the 2d map
		//nxt_Block_Moving_Vec = branchDirection;
		Block nxtBlock;
		float prev_block_hight = 0;
		int nextx, nexty;
		MapBlock currMapBlock;

		/**********************************/
		/* world creation*/
		/**********************************/
		//nxtBlockLOcation = currBlockLOcation;
		Block currBlock = frankPosition;
		nextx = currBlock.map_x;
		nexty = currBlock.map_y;

        /**********************************/
        //place rest of the blocks
        /**********************************/
        for (int i = 0; i < blocksPerBranch; i++)
        {
            prevAngleOfPlacing = currentAngleOfPlacing;

            if (currBlock.getType() == Type.hallwayTurnR)
            {
                currentAngleOfPlacing.eulerAngles += new Vector3(0, 90, 0);
                nxt_Block_Moving_Vec = Quaternion.Euler(0, 90, 0) * nxt_Block_Moving_Vec;
            }
            else if (currBlock.getType() == Type.hallwayTurnL)
            {
                currentAngleOfPlacing.eulerAngles -= new Vector3(0, 90, 0);
                nxt_Block_Moving_Vec = Quaternion.Euler(0, -90, 0) * nxt_Block_Moving_Vec;
            }

            else if (currBlock.getType() == Type.mysteryRoom)
            {
                mysteryRoomsCount++;
            }
            else if (currBlock.getType() == Type.Tjunction)
            {
                currentAngleOfPlacing.eulerAngles += new Vector3(0, 90, 0);
            }

            nxtBlockLOcation += nxt_Block_Moving_Vec;
            nxtBlock = Instantiate(pickNextBlock(currBlock), nxtBlockLOcation, Quaternion.Euler(0, -90, 0));
            nxtBlock.blockInit(dm, this);

            //sets the 2d map coords for the blocks, and sets them in the 2d map as well
            /**********************************/
            nextx = Mathf.RoundToInt((nxt_Block_Moving_Vec.x) / (currBlock.length));
            nexty = Mathf.RoundToInt((nxt_Block_Moving_Vec.z) / (currBlock.length));
            nextx += (int)currBlock.map_x;
            nexty += (int)currBlock.map_y;
            nxtBlock.setXY(nextx, nexty);
            ////////////////////////////////////////////////////////
            // I'm changing from here, Aviad: |---------------------------------------------------------------
            Block tempSwitchedBlock;

            //if we just placed a turn left block into a wall
            if (currBlock.getType() == Type.hallwayTurnL)
            {
                int nextTurnx = Mathf.RoundToInt((nxt_Block_Moving_Vec.x) / (currBlock.length));
                int nextTurny = Mathf.RoundToInt((nxt_Block_Moving_Vec.z) / (currBlock.length));
                Vector2 TurnLVec = new Vector2(nextTurnx, nextTurny);
                TurnLVec = Quaternion.Euler(0, -90, 0) * TurnLVec;
                if ((map2d[nxtBlock.map_x, nxtBlock.map_y].Count != 0) && map2d[nxtBlock.map_x, nxtBlock.map_y]
                    [map2d[nxtBlock.map_x, nxtBlock.map_y].Count - 1].getfloor() == currFloor)
                {/*
					Block temp = pickNextBlock(currBlock);
					while (temp.getType() == Type.hallwayTurnL)         //CHANGED "!=" TO "=="
					{
						temp = pickNextBlock(currBlock);
					}
                    tempSwitchedBlock = Instantiate (temp, nxtBlockLOcation, Quaternion.Euler(0, -90, 0));      //CHANGED "nxtBlock" TO "tempSwitchedBlock"
                    tempSwitchedBlock.blockInit(dm);        //CHANGED line added
                    tempSwitchedBlock.setXY(nxtBlock.map_x, nxtBlock.map_y);        //CHANGED line added
                    Destroy (nxtBlock.gameObject);
                    nxtBlock = tempSwitchedBlock;        //CHANGED line added
                    */

                    Block temp = Instantiate(initialHallBlock, currBlock.transform.position, currBlock.transform.rotation);
                    currentAngleOfPlacing = prevAngleOfPlacing;
                    nxt_Block_Moving_Vec = currentAngleOfPlacing * new Vector3(currBlock.length, 0, 0);
                    nxtBlockLOcation = currBlockLOcation + nxt_Block_Moving_Vec;
                    nxtBlock.transform.position = nxtBlockLOcation;
                    //nxtBlock.transform.Rotate(new Vector3(0, 90, 0));
                    nxtBlock.setXY(currBlock.map_x + Mathf.RoundToInt((nxt_Block_Moving_Vec.x) / (currBlock.length)), currBlock.map_y + Mathf.RoundToInt((nxt_Block_Moving_Vec.z) / (currBlock.length)));
                    temp.setXY(currBlock.map_x, currBlock.map_y);
                    temp.blockIndex = currBlock.blockIndex;
                    Destroy(currBlock.gameObject);
                    currBlock = temp;
                    currBlock.blockInit(dm, this);
                    blockMapLocation = new Vector2(nxtBlock.map_x, nxtBlock.map_y);
                }
            }
            //if we just placed a turn right block into a wall
            if (currBlock.getType() == Type.hallwayTurnR)
            {
                int nextTurnx = Mathf.RoundToInt((nxt_Block_Moving_Vec.x) / (currBlock.length));
                int nextTurny = Mathf.RoundToInt((nxt_Block_Moving_Vec.z) / (currBlock.length));
                Vector2 TurnLVec = new Vector2(nextTurnx, nextTurny);
                TurnLVec = Quaternion.Euler(0, 90, 0) * TurnLVec;
                if ((map2d[nxtBlock.map_x, nxtBlock.map_y].Count != 0) && map2d[nxtBlock.map_x, nxtBlock.map_y]
                    [map2d[nxtBlock.map_x, nxtBlock.map_y].Count - 1].getfloor() == currFloor)
                {
                    /*
                        Block temp = pickNextBlock(currBlock);
                        while (temp.getType() == Type.hallwayTurnR)         //CHANGED "!=" TO "=="
                        {
                            temp = pickNextBlock(currBlock);
                        }
                        tempSwitchedBlock = Instantiate (temp, nxtBlockLOcation, Quaternion.Euler(0, -90, 0));      //CHANGED "nxtBlock" TO "tempSwitchedBlock"
                        tempSwitchedBlock.blockInit(dm);        //CHANGED line added
                        tempSwitchedBlock.setXY(nxtBlock.map_x, nxtBlock.map_y);        //CHANGED line added
                        Destroy(nxtBlock.gameObject);
                        nxtBlock = tempSwitchedBlock;        //CHANGED line added
                        */
                    
                    Block temp = Instantiate(initialHallBlock, currBlock.transform.position, currBlock.transform.rotation);
                    currentAngleOfPlacing = prevAngleOfPlacing;
                    nxt_Block_Moving_Vec = currentAngleOfPlacing * new Vector3(currBlock.length, 0, 0);
                    nxtBlockLOcation = currBlockLOcation + nxt_Block_Moving_Vec;
                    nxtBlock.transform.position = nxtBlockLOcation;
                    //nxtBlock.transform.Rotate(new Vector3(0, -90, 0));
                    nxtBlock.setXY(currBlock.map_x + Mathf.RoundToInt((nxt_Block_Moving_Vec.x) / (currBlock.length)), currBlock.map_y + Mathf.RoundToInt((nxt_Block_Moving_Vec.z) / (currBlock.length)));
                    temp.setXY(currBlock.map_x, currBlock.map_y);
                    temp.blockIndex = currBlock.blockIndex;
                    Destroy(currBlock.gameObject);
                    currBlock = temp;
                    currBlock.blockInit(dm, this);
                    blockMapLocation = new Vector2(nxtBlock.map_x, nxtBlock.map_y);
                }
            }

            //if the picked block is a junction but we have too many junctions pick 
            if (((nxtBlock.getType() == Type.Tjunction) && canAddJunction == false))
            {
                Block temp = null;      //CHANGED line added
                while ((nxtBlock.getType() == Type.Tjunction))         //CHANGED "!=" TO "=="
                {
                    temp = pickNextBlock(currBlock);      //CHANGED "nxtBlock" TO "tempSwitchedBlock"
                }
                tempSwitchedBlock = Instantiate(temp, nxtBlockLOcation, Quaternion.Euler(0, -90, 0));      //CHANGED line added
                tempSwitchedBlock.blockInit(dm, this);        //CHANGED line added
                tempSwitchedBlock.setXY(nxtBlock.map_x, nxtBlock.map_y);        //CHANGED line added
                Destroy(nxtBlock.gameObject);
                nxtBlock = tempSwitchedBlock;        //CHANGED line added
            }

            //add next block as i=the chosen block or a deadend
            /**********************************/
            if (i == blocksPerBranch - 1)
            {
                currBlock = Instantiate (nxtBlock.turnMeToDeadEnd, nxtBlockLOcation, Quaternion.Euler(0, -90, 0));
                Destroy(nxtBlock.gameObject);
                currBlock.blockInit(dm, this);
                branchEnd = currBlock;
            }
            else
            {
                //checks for floor update
                if ((map2d[(nxtBlock.map_x), (nxtBlock.map_y)].Count != 0) && (map2d[(nxtBlock.map_x), (nxtBlock.map_y)])[map2d[(nxtBlock.map_x), (nxtBlock.map_y)].Count - 1].getfloor()
                    == currFloor)
                {
                    currFloor++;
                    prev_block_hight = currBlock.hight;
                    Block tempCurr = Instantiate(stairs, currBlockLOcation, Quaternion.Euler(0, -90, 0));
                    tempCurr.blockInit(dm, this);
                    tempCurr.setXY(currBlock.map_x, currBlock.map_y);
                    Debug.Log(currBlock.blockIndex);
                    Destroy(currBlock.gameObject);
                    currBlock = tempCurr;
                    currBlock.blockIndex = blockCounter;
                    //Instantiate (currBlock, currBlockLOcation, Quaternion.identity);
                    nxtBlockLOcation += (new Vector3(0, prev_block_hight, 0));
                    nxtBlock.transform.position = nxtBlockLOcation;
                    currBlock.transform.Rotate(prevAngleOfPlacing.eulerAngles);
                    currBlock = nxtBlock;
                    //currBlock.transform.Rotate(prevAngleOfPlacing.eulerAngles);
                    currMapBlock = new MapBlock();
                    currMapBlock.setAlive(true);
                    currMapBlock.setfloor(currFloor);
                    currMapBlock.setIndex(blockCounter);
                    map2d[(int)blockMapLocation.x, (int)blockMapLocation.y].Add(currMapBlock);

                }
                // no floor update
                else
                {
                    currBlock = nxtBlock;
                    if (nxtBlock.getType() == Type.Tjunction && canAddJunction)
                    {
                        canAddJunction = false;
                        Block junctionBlockLeft = pickNextBlock(currBlock);
                        while (junctionBlockLeft.getType() != Type.hallwayStright)
                        {
                            junctionBlockLeft = pickNextBlock(currBlock);
                        }
                        junctionBlockLeft = Instantiate(junctionBlockLeft,
                            nxt_Block_Moving_Vec - (new Vector3(90, 0, 0)), Quaternion.Euler(0, -90, 0));
                        junctionBlockLeft.blockInit(dm, this);
                        junctionBlockLeft.blockIndex = blockCounter + 1;
                        int nextxBranch = Mathf.RoundToInt((nxt_Block_Moving_Vec.x) / (currBlock.length));
                        int nextyBranch = Mathf.RoundToInt((nxt_Block_Moving_Vec.z) / (currBlock.length));
                        nextxBranch += (int)currBlock.map_x;
                        nextyBranch += (int)currBlock.map_y;
                        junctionBlockLeft.setXY(nextxBranch, nextyBranch);
                        junctionBlockLeft.setFloor(currFloor);
                        MapBlock mapblock = new MapBlock();
                        mapblock.setAlive(true);
                        mapblock.setfloor(currFloor);
                        mapblock.setIndex(blockCounter + 1);
                        map2d[junctionBlockLeft.map_x, junctionBlockLeft.map_y].Add(mapblock);
                        junctionBlockLeft.transform.Rotate(junctionLeftAngleOfPlacing.eulerAngles + (new Vector3(90, 0, 0)));
                        addBranchHelper(junctionBlockLeft, nxt_Block_Moving_Vec - (new Vector3(90, 0, 0)));

                    }
                }
            }

            /**********************************/
            currBlock.setFloor(currFloor);
            currBlock.setBlockVec(nxt_Block_Moving_Vec);
            blocksList.Add(currBlock);


            if (i == blocksPerBranch - 1)
            {
                blocksOfMaxIDX.Add(currBlock);
            }
            if (currBlock.getType() == Type.mysteryRoom)
            {
                mysteryRooms.Add(currBlock);
            }
            currBlock.transform.Rotate(currentAngleOfPlacing.eulerAngles);
            currBlockLOcation = nxtBlockLOcation;
            blockCounter += 1;
            currBlock.blockIndex = blockCounter;
            //sets the block in the 2d map

            blockMapLocation.x += nxt_Block_Moving_Vec.x / currBlock.length;
            blockMapLocation.y += nxt_Block_Moving_Vec.z / currBlock.length;
            currMapBlock = new MapBlock();
            currMapBlock.setAlive(true);
            currMapBlock.setIndex(blockCounter);
            currMapBlock.setfloor(currFloor);
            map2d[currBlock.map_x, currBlock.map_y].Add(currMapBlock);
            currMapBlock.setAlive(true);
            currMapBlock.setIndex(blockCounter);


        }
        /**********************************/
        List<int> l = new List<int>();
		foreach (Block b in blocksOfMaxIDX)
		{
			l.Add(b.blockIndex);
		}
		return l;
	}
	////////////////////////////////////////////
}
